��          �   %   �      0  %   1  ,   W     �     �      �     �     �     �          
     #     +     2     :     X     `  %   o     �     �     �     �     �  �   �  \  �  -      8   .     g     w  "   �     �     �     �  	   �      �       	        (  !   5  
   W     b  '   y     �     �     �     �     �  �                                                                                       	                    
                       Add POIs: markers, lines, polygons... Batch import geostructured data (GEOJson...) Browse maps Choose the layers of your map Choose the licence for your data Choose your tilelayer Create your map now! Cured by Delete Embed and share your map Fork it Log in Log out Manage POIs colours and icons My maps Not map found. Search for maps containing «%(q)s»  Search maps See this map! Test it and create a map Users to play with What you can do? u{Map} let you create maps with OpenStreetMap layers in a minute and embed them in your site.<br /> This is a demo instance, you can host your own, it's <strong>open source</strong>! Project-Id-Version: 0.0.8
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-12-27 14:08+0100
PO-Revision-Date: 2012-12-27 11:09+0100
Last-Translator: YOHAN BONIFACE <yb@enix.org>
Language-Team: FR <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1)
 Ajouter des POI: markes, lignes, polygones... Import des données géographiques en masse (GeoJSON...) Voir des cartes Choisir les fonds de carte Choisir la licence de vos données Choisir un fond de carte Créer une carte Maintenu par Supprimer Exporter et partager votre carte Forker Connexion Déconnexion Choisir la couleur et les icônes Mes cartes Aucune carte trouvée. Chercher des cartes contenant «%(q)s» Chercher des cartes Voir cette carte! Tester et créer une carte Utilisateurs pour tester Que pouvez-vous faire? u{Map} permet de créer des cartes personnalisées sur des fonds OpenStreetMap en un instant et les afficher dans votre site <br /> Ceci est une instance de démonstration, vous pouvez héberger la vôtre, c'est <strong>open source</strong>! 